# http://www.ratbv.ro/js/routes.js

$(document).ready(function() {
    var styles = [{
            stylers: [{
                    hue: "#1079B6"
                }, {
                    saturation: -20
                }]
        }, {
            featureType: "road",
            elementType: "geometry",
            stylers: [{
                    lightness: 100
                }, {
                    visibility: "simplified"
                }]
        }, {
            featureType: "poi",
            elementType: "labels",
            stylers: [{
                    visibility: "off"
                }]
        }, {
            featureType: "transit",
            elementType: "labels",
            stylers: [{
                    visibility: "off"
                }]
        }];

    // Draw map
    $("#map").show("slow", function() {
        map = new GMaps({
            el: '#map',
            styles: styles,
            lat: 45.667115,
            lng: 25.612599,
            zoom: 13,
            zoomControl: true,
            zoomControlOpt: {
                style: 'SMALL',
                position: 'TOP_LEFT'
            },
            panControl: false,
        });

        // Draw route
        $.ajax({
            type: "POST",
            url: "/ajax/route.php",
            data: {"transportation_line_id": $("#transportation_line_id").val(), "tour_type": $("#tour_type").val()},
            dataType: "JSON",
            cache: false,
            success: function(data) {
                var origin = [];
                var destination = [];
                var waypoints = [];
                       
                $.each(data, function(key, value) {
                    // Add start station marker
                    if (key == "start_station") {
                        map.addMarker({
                            lat: value.latitude,
                            lng: value.longitude,
                            icon: '/images/marker-busstop.png',
                            infoWindow: {
                               content: '<div class="info-window"><h3>' + value.name + '</h3><p>Linii: ' + value.lines + '</p></div>'
                            }
                        });
                        
                        origin = [value.latitude, value.longitude];
                    }       
                    
                    // Add end station marker
                    if (key == "end_station") {
                        map.addMarker({
                            lat: value.latitude,
                            lng: value.longitude,
                            icon: '/images/marker-busstop.png',
                            infoWindow: {
                                content: '<div class="info-window"><h3>' + value.name + '</h3><p>Linii: ' + value.lines + '</p></div>'
                            }
                        });
                        
                        destination = [value.latitude, value.longitude];
                    }  
                    
                    if (key == "waypoints") {
                        $.each(value, function(subkey, subvalue) {
                            map.addMarker({
                                lat: subvalue.latitude,
                                lng: subvalue.longitude,
                                icon: '/images/marker-busstop.png',
                                infoWindow: {
                                   content: '<div class="info-window"><h3>' + subvalue.name + '</h3><p>Linii: ' + subvalue.lines + '</p></div>'
                                }
                            });
                            
                            waypoints.push({
                                location: new google.maps.LatLng(subvalue.latitude, subvalue.longitude),
                                stopover: false
                            });
                        });
                    }
                });
                                
                /*
                map.travelRoute({
                    origin: origin,
                    destination: destination,
                    travelMode: 'driving',
                    waypoints: waypoints,
                    step: function(e) {
                        $('#instructions').append('<li>' + e.instructions + '</li>');
                        $('#instructions li:eq(' + e.step_number + ')').delay(450 * e.step_number).fadeIn(200, function() {
                            map.drawPolyline({
                                path: e.path,
                                strokeColor: '#FF0000',
                                strokeOpacity: 1,
                                strokeWeight: 5
                            });
                        });
                    }
                });
                */
            }
        });
        
        // Array to hold points of interest markers
        var poi_markers = [];

        // Get points of interest and display them on the map
        $.ajax({
            type: "POST",
            url: "/ajax/poi.php",
            dataType: "JSON",
            cache: false,
            success: function(data) {
                $.each(data, function(key, value) {
                    poi_marker = {
                        lat: value.latitude,
                        lng: value.longitude,
                        icon: '/images/marker-sight.png',
                        infoWindow: {
                            content: '<div class="info-window"><h3>' + value.name + '</h3><p>' + value.address + '</p></div>'
                        }
                    }
                    poi_markers.push(poi_marker);
                });
            }
        });

        // Check if the user wants to display points of interest
        $("#poi").click(function() {
            if ($("#poi").is(':checked')) {
                map.addMarkers(poi_markers);
            } else {
                // TODO: remove markers
            }
        });

        // Check if the user wants to display tickets centers
        $("#tickets").click(function() {

            if ($("#tickets").is(':checked')) {
                // Get tickets centers and display them on the map
                $.ajax({
                    type: "POST",
                    url: "/ajax/tickets-centers.php",
                    dataType: "JSON",
                    cache: false,
                    success: function(data) {
                        $.each(data, function(key, value) {
                            map.addMarker({
                                lat: value.latitude,
                                lng: value.longitude,
                                icon: '/images/marker-kiosk.png',
                                infoWindow: {
                                    content: '<div class="info-window"><h3>' + value.name + '</h3><p>Program: 06.00 - 20.00</p></div>'
                                }
                            });
                        });
                    }
                });
            } else {
                // TODO: remove markers
            }
        });

    });

}); 
