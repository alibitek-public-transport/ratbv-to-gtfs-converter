#!/usr/bin/env bash

for i in {1..100}; do 
    echo $i; 
    
    curl -s 'http://www.ratbv.ro/ajax/route.php' -H 'Pragma: no-cache' -H 'Origin: http://www.ratbv.ro' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.8' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Cache-Control: no-cache' -H 'X-Requested-With: XMLHttpRequest' -H 'Cookie: PHPSESSID=3574cd2bc4d1fa6ce9c9469a3173533d' -H 'Connection: keep-alive' --data "transportation_line_id=$i&tour_type=retour" --compressed | python2.7 -mjson.tool > retour_$i.json
    
    sleep 0.01
done 

# find . -type f -size -126c -exec mv {} ../wrong/ \;
# 32 Tours: ag -os '"number": "\d+"' | cut -d':' -f4 | tr -d ' "' | wc -l
# 32 Retours: ag -os '"number": "\d+"' retour_*.json | cut -d':' -f4 | tr -d ' "' | wc -l
