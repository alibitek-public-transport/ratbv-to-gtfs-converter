#!/usr/bin/env bash

set -x

containsElement () {
  local e
  for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
  return 1
}

body() {
    IFS=, read -r header
    printf '%s\n' "$header"
    "$@"
}

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

BASE_URL="http://www.ratbv.ro"
OUTPUT_DIR="$DIR/brasov_gtfs"
PROCESS_DIR="$DIR/tmp"
PREVIOUS_GTFS_FEED_DIR="$DIR/old_data"

if [[ $(stat -c '%Y' "$PROCESS_DIR/trasee_si_orare.html") -le $(($(date +%s)-86400)) ]]; then
    echo "Fetching trasse_si_orare.html"
    curl -s 'http://www.ratbv.ro/trasee-si-orare/' > "$PROCESS_DIR/trasee_si_orare.html"
fi

if [[ $(stat -c '%Y' "$PROCESS_DIR/style.css") -le $(($(date +%s)-86400)) ]]; then
    echo "Fetching style.css"
    curl -s 'http://www.ratbv.ro/css/style.css' > "$PROCESS_DIR/style.css"
fi

if [[ ! -d "$OUTPUT_DIR" ]]; then
 mkdir -p "$OUTPUT_DIR"
fi

if [[ ! -d "$PROCESS_DIR" ]]; then
 mkdir -p "$PROCESS_DIR"
fi

agency_id="RATBV"

echo "Harta trasee: ${BASE_URL}$(xidel --data="$PROCESS_DIR/trasee_si_orare.html" -q -e '/html/body/div[2]/div[3]/div[2]/p[3]/strong/span/a/@href')"

# Get transport types: Autobuze / Autobuze Midi / Troleibuze
IFS=';' read -a transport_types <<< "$(xidel --data="$PROCESS_DIR/trasee_si_orare.html" -q -e '//table/tbody/tr[1]' | awk -F'\n' '{ if (NR > 1) { var=var ";" $1; } else { var=var $1; } } END { print var; }')" 
# or xidel -q --data tmp/trasee_si_orare.html -e '/html/body/div[2]/div[3]/div[2]/table/tbody/tr[1]'

# psql -t -q -A -X -w -h localhost -U postgres -d gtfs

# Autobuze 27
# Autobuze Midi 9
# Troleibuze 5
# Total: 41

# Show routes per transport type: 
# Autobuze: xidel -q --data tmp/trasee_si_orare.html -e '/html/body/div[2]/div[3]/div[2]/table/tbody/*/td[1]'
# Autobuze midi: xidel -q --data tmp/trasee_si_orare.html -e '/html/body/div[2]/div[3]/div[2]/table/tbody/*/td[2]'
# Troleibuze: xidel -q --data tmp/trasee_si_orare.html -e '/html/body/div[2]/div[3]/div[2]/table/tbody/*/td[3]'

for index in "${!transport_types[@]}"
do
    index=$(($index+1))
    echo -e "\n$index ${transport_types[$((index-1))]}"
    
    # Number of routes for this transport type (e.g. number of routes by bus)
    number_of_routes=$(xidel --data="$PROCESS_DIR/trasee_si_orare.html" -q -e "//table/tbody/tr[position() > 1]/td[$index]" | grep '^Linia' | wc -l)

    IFS=';' read -a route_long_names <<< "$(xidel --data="$PROCESS_DIR/trasee_si_orare.html" -q -e "//table/tbody/tr[position() > 1]/td[$index]//*/text()" | grep -vP '^Linia|^$' | sed 's/\xC2\xA0//g' | sed '/^\s*$/d' | awk -F'\n' '{ if (NR > 1) { var=var ";" $1; } else { var=var $1; } } END { print var; }')" 
    
    # A longer version of the above, but it requires even more specific case handling 
    #IFS=';' read -a route_long_names <<< "$(xidel --data="$PROCESS_DIR/trasee_si_orare.html" -q -e "//table/tbody/tr[position() > 1]/td[$index]/strong/span[2]|//table/tbody/tr[position() > 1]/td[$index]/strong/span/span[2]|//table/tbody/tr[position() > 1]/td[$index]/span/strong/span|//table/tbody/tr[position() > 1]/td[$index]/strong[2]/span|//table/tbody/tr[position() > 1]/td[$index]/span/span/strong|//table/tbody/tr[position() > 1]/td[$index]/strong/span/span/span/strong/span|//table/tbody/tr[position() > 1]/td[$index]/strong/span/span/strong/span|//table/tbody/tr[position() > 1]/td[$index]/strong/span/strong/span/span[2]|//table/tbody/tr[position() > 1]/td[$index]/strong/span/span/strong/span/span[2]" | grep -v '^Linia' | sed 's/\xC2\xA0//g' | sed '/^\s*$/d' | awk -F'\n' '{ if (NR > 1) { var=var ";" $1; } else { var=var $1; } } END { print var; }')"        
    
    # Route short names (e.g. 32)
    route_short_names=($(xidel --data="$PROCESS_DIR/trasee_si_orare.html" -q -e "//table/tbody/tr[position() > 1]/td[$index]//*/a/text()" | sed 's/Linia //'))
    
    # Route link on the website
    route_links=($(xidel --data="$PROCESS_DIR/trasee_si_orare.html" -q -e "//table/tbody/tr[position() > 1]/td[$index]//*/a/@href"))
    
    # Route CSS style
    route_styles=($(xidel --data="$PROCESS_DIR/trasee_si_orare.html" -q -e "//table/tbody/tr[position() > 1]/td[$index]//*/a/@class"))        

    printf '~>Expected number of short route names: %d\n~>Actual number of short route names: %d\n' $number_of_routes "${#route_short_names[@]}"
    printf '%s\n' "${route_short_names[@]}"
    if [[ $number_of_routes -ne ${#route_short_names[@]} ]]; then
        echo "Expected != actual number of - short - routes"
        exit 1
    fi

    printf '~>Expected number of long route names: %d\n~>Actual number of long route names: %d\n' $number_of_routes "${#route_long_names[@]}"
    printf '%s\n' "${route_long_names[@]}"
    if [[ $number_of_routes -ne ${#route_long_names[@]} ]]; then
        echo "Expected != actual number of - long - routes"
        exit 1
    fi

    printf '~>Expected number of route links: %d\n~>Actual number of route links: %d\n' $number_of_routes "${#route_links[@]}"
    printf '%s\n' "${route_links[@]}"
    if [[ $number_of_routes -ne ${#route_links[@]} ]]; then
        echo "Expected != actual number of - links - routes"
        exit 1
    fi

    printf '~>Expected number of route styles: %d\n~>Actual number of route styles: %d\n' $number_of_routes "${#route_styles[@]}"
    printf '%s\n' "${route_styles[@]}"
    if [[ $number_of_routes -ne ${#route_styles[@]} ]]; then
        echo "Expected != actual number of - styles - routes"
        exit 1
    fi

    # Uncomment this line to make sure the first page is correctly parsed in full
    #continue

    if [[ $index -eq 1 ]]; then
        > colors_missing.txt

        # X feed_info.txt - https://developers.google.com/transit/gtfs/reference#feed_infotxt
        echo "feed_publisher_name,feed_publisher_url,feed_lang,feed_start_date,feed_end_date,feed_version" > "$OUTPUT_DIR/feed_info.txt"
        echo "Alex Butum,http://linuxgeek.ro,ro,20150914,20160914,$(date +%s)" >> "$OUTPUT_DIR/feed_info.txt"
        # @TODO: Find a way to determine the feed_start_date and feed_end_date from ratbv website

        # X agency.txt - https://developers.google.com/transit/gtfs/reference#agencytxt
        echo "agency_id,agency_name,agency_url,agency_timezone,agency_lang,agency_phone,agency_fare_url,agency_email" > "$OUTPUT_DIR/agency.txt"
        echo "RATBV,Regia Autonomă de Transport Brașov,http://www.ratbv.ro,Europe/Bucharest,RO,0368.800.600,http://www.ratbv.ro/tarife-bilete-si-abonamente/,ratbv@ratbv.ro" >> "$OUTPUT_DIR/agency.txt"

        # X fare_attributes.txt - https://developers.google.com/transit/gtfs/reference#fare_attributestxt
        # Read: http://www.ratbv.ro/tarife-bilete-si-abonamente/ && http://www.ratbv.ro/intrebari-frecvente-bilet-valabil-50-min/
        echo "fare_id,price,currency_type,payment_method,transfers,transfer_duration" > "$OUTPUT_DIR/fare_attributes.txt"
        echo "1,4,RON,1,,3000" >> "$OUTPUT_DIR/fare_attributes.txt"
        echo "2,5,RON,1,0," >> "$OUTPUT_DIR/fare_attributes.txt"
        # @TODO: Get the data by crawling the above mentioned links

        # X fare_rules.txt - https://developers.google.com/transit/gtfs/reference#fare_rulestxt
        echo "fare_id,route_id,origin_id,destination_id,contains_id" > "$OUTPUT_DIR/fare_rules.txt"

        # X routes.txt - https://developers.google.com/transit/gtfs/reference#routestxt
	    echo "route_id,agency_id,route_short_name,route_long_name,route_desc,route_type,route_url,route_color,route_text_color" > "$OUTPUT_DIR/routes.txt"

        # stops.txt - https://developers.google.com/transit/gtfs/reference#stopstxt
	    echo "stop_id,stop_name,stop_lat,stop_lon" > "$OUTPUT_DIR/stops.txt"

        # calendar.txt - https://developers.google.com/transit/gtfs/reference#calendartxt
	    echo 'service_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,start_date,end_date' > "$OUTPUT_DIR/calendar.txt"

        # stop_times.txt - https://developers.google.com/transit/gtfs/reference#stop_timestxt
	    echo 'trip_id,arrival_time,departure_time,stop_id,stop_sequence,pickup_type,drop_off_type' > "$OUTPUT_DIR/stop_times.txt"
    fi

    #declare -a transport_line_ids

    if [[ ! -d "$PROCESS_DIR/stops" ]]; then
      mkdir -p "$PROCESS_DIR/stops"
    fi

    if [[ ! -d "$PROCESS_DIR/stops/tour/json" ]]; then
      mkdir -p "$PROCESS_DIR/stops/tour/json"
    fi

    if [[ ! -d "$PROCESS_DIR/stops/tour/csv" ]]; then
      mkdir -p "$PROCESS_DIR/stops/tour/csv"
    fi

    if [[ ! -d "$PROCESS_DIR/stops/retour/json" ]]; then
      mkdir -p "$PROCESS_DIR/stops/retour/json"
    fi

    if [[ ! -d "$PROCESS_DIR/stops/retour/json" ]]; then
      mkdir -p "$PROCESS_DIR/stops/retour/csv"
    fi

    for rindex in "${!route_short_names[@]}" 
    do	
        # ------------------------------------------------------------------------------------------------
        # routes.txt

        route_short_name="${route_short_names[$rindex]}"
        route_long_name="${route_long_names[$rindex]}"
        route_url="${BASE_URL}${route_links[$rindex]}"
        route_style="${route_styles[$rindex]}"

        # White background
        route_color="FFFFFF"

        route_text_color=$(grep -iP ".continut-pagina a.${route_style}:before[ ,]*{?" -A 3 "$PROCESS_DIR/style.css" | grep -P 'color\s*:\s* #[a-zA-Z0-9]+;' | tail -n 1 | cut -d: -f2 | sed 's/[ ;]//g')
        if [[ -z $route_color ]]; then
            route_text_color='2391d0'

            if [[ $route_short_name = "23" ]] ; then
                route_text_color="2f4f4f"
            fi

            echo "Can't find color for route: $route_short_name" >> colors_missing.txt
        fi

        route_text_color=${route_text_color//#}

        route_color=$(echo $route_color | tr "[:lower:]" "[:upper:]")
        route_text_color=$(echo $route_text_color | tr "[:lower:]" "[:upper:]")

        # All are buses
        route_type=3

        echo "$route_short_name,$agency_id,$route_short_name,$route_long_name,L$route_short_name,$route_type,$route_url,$route_color,$route_text_color" >> "$OUTPUT_DIR"/routes.txt
        # ------------------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------------------
        # fare_rules.txt

        if [[ $route_short_name != "20" ]]; then
          fare_id=1
        else
          fare_id=2
        fi
        echo "$fare_id,$route_short_name,,," >> "$OUTPUT_DIR"/fare_rules.txt
        # ------------------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------------------
        # stops.txt

        # skip routes for which the http://www.ratbv.ro/ajax/route.php doesn't provide data
        excluded_routes=(23 3 7 50)
        containsElement "$route_short_name" "${excluded_routes[@]}"
        if [[ $? -eq 0 ]]; then
            continue
        fi

        if [[ $route_url == *"afisaje"* ]]; then
            case "$route_short_name" in
                "25")
                    transportation_line_id=23
                ;;
                "37")
                    transportation_line_id=32
                ;;
                "28B")
                    transportation_line_id=46
                ;;
                "52")
                    transportation_line_id=43
                ;;
                "5")
                    transportation_line_id=35
                ;;
                "18")
                    transportation_line_id=36                    
                ;;                
                "28")
                    transportation_line_id=38
                ;;
                "33")
                    transportation_line_id=28
                ;;
                "34")
                    transportation_line_id=39
                ;;
#                "50")
#                    transportation_line_id=44 # WARNING: tours returns empty response, retour returns only start and end which are the same stop...
#                ;;
                *)
                transportation_line_id=$route_short_name
                ;;
            esac            
        else
          transportation_line_id=$(echo -n "$route_url" | grep -Po '\d+')
        fi

        for route_type in tour retour
        do
            if [[ ! -d "$PROCESS_DIR/stops/$route_type/json" ]]; then
                mkdir -p "$PROCESS_DIR/stops/$route_type/json"
            fi

            if [[ ! -d "$PROCESS_DIR/stops/$route_type/csv" ]]; then
                mkdir -p "$PROCESS_DIR/stops/$route_type/csv"
            fi

            if [[ ! -f "$PROCESS_DIR/stops/$route_type/json/$route_short_name.json" || $(stat -c '%Y' "$PROCESS_DIR/stops/$route_type/json/$route_short_name.json") -le $(($(date +%s)-86400)) ]]; then
                curl -s 'http://www.ratbv.ro/ajax/route.php' -H 'Host: www.ratbv.ro' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:41.0) Gecko/20100101 Firefox/41.0' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'DNT: 1' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data "transportation_line_id=$transportation_line_id&tour_type=$route_type" | python2.7 -mjson.tool > "$PROCESS_DIR/stops/$route_type/json/$route_short_name.json"
            fi

            jq -er '[.start_station|.code,.name,(.latitude|sub(",";".")),(.longitude|sub(",";"."))] | @csv' "$PROCESS_DIR/stops/$route_type/json/$route_short_name.json" | sed 's/"//g' | sed '/^\s*$/d' > "$PROCESS_DIR/stops/$route_type/csv/$route_short_name.csv"
            [[ $? -ne 0 ]] && echo "$route_short_name" >> "$PROCESS_DIR/jq_problems.txt"

            jq -er '.waypoints[] | [.code,.name,(.latitude|sub(",";".")),(.longitude|sub(",";"."))] | @csv' "$PROCESS_DIR/stops/$route_type/json/$route_short_name.json" | sed 's/"//g' | sed '/^\s*$/d' >> "$PROCESS_DIR/stops/$route_type/csv/$route_short_name.csv"
            jq -er '[.end_station|.code,.name,.latitude,.longitude] | @csv' "$PROCESS_DIR/stops/$route_type/json/$route_short_name.json" | sed 's/"//g' | sed '/^\s*$/d' >> "$PROCESS_DIR/stops/$route_type/csv/$route_short_name.csv"

            #containsElement "$transportation_line_id" "${transport_line_ids[@]}"
            #if [[ $? -ne 0 ]]; then
                #transport_line_ids+=("$transportation_line_id")
                sed -i 's/\\0//g' "$PROCESS_DIR/stops/$route_type/csv/$route_short_name.csv"
                cat "$PROCESS_DIR/stops/$route_type/csv/$route_short_name.csv" >> "$OUTPUT_DIR/stops.txt"
            #fi
        done

        # @TODO: Problem with: 3, 7, 25, 28B, 50, 52, but grep -c afisaje routes.txt yields 12 rutes with afisaje
        # curl -s 'http://www.ratbv.ro/ajax/route.php' -H 'Host: www.ratbv.ro' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:41.0) Gecko/20100101 Firefox/41.0' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'DNT: 1' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data "transportation_line_id=34&tour_type=tour"

        # Find routes with /route/ URLs
        # awk -F',' 'NR > 1 {print $7}' routes.txt  | grep route | grep -Po '\d+' | sort -g | xargs
        # 1 2 4 5 9 13 14 15 16 17 18 19 20 21 22 24 26 27 29 30 31 32 33 34 48
        
        # Find routes with /afisaje URLs
        # awk -F',' 'NR > 1 {print $7}' routes.txt  | grep afisaje | grep -Po '\d+' | sort -g | xargs
        # 3 5 6 7 8 18 23 25 28 28B 33 34 41 50 52
        
        # TODO: can't find stop coordinates for routes
        # 23, ->
        # 3, ->
        # 7, ->
        # 50, -> 44 empty response

        # Stops unique to previous version:
        # comm -13 <(tail -n +1 brasov_gtfs/stops.txt | cut -d, -f1 | sort -u) <(tail -n +1 old_data/brasov/stops.txt | cut -d, -f1 | sort -u)

        # ------------------------------------------------------------------------------------------------
        # calendar.txt

        # Find number of timetables in new format - 26
        # grep -c route/ routes.txt

        # Find routes with timetables in new format
        # awk -F',' '$7 ~ /route\// {print $1}' routes.txt | sort -u

        # Find number of timetables in old format - 15
        # grep -c afisaje/ routes.txt

        # Find routes with timetables in old format
        # awk -F',' '$7 ~ /afisaje\// {print $1}' routes.txt | sort -u

        # Compare generate calendar dates (# NOTE: This should match exactly, otherwise, we have a problem!)
        # grep -Po '(?<=WEEK_)\w+' calendar.txt | grep -v '_HOLIDAY' | sort -u
        # vs
        # awk -F',' '$7 ~ /route\// {print $1}' routes.txt | sort -g
        # 26 = 26

        if [[ ! -d "$PROCESS_DIR/timetables/${route_short_name}" ]]; then
              mkdir -p "$PROCESS_DIR/timetables/${route_short_name}"
        fi

        # new timetable page format
        if [[ $route_url = *"tour"* ]]; then
            timetable_url=${route_url}timetable/
            timetable_file="$PROCESS_DIR/timetables/${route_short_name}/timetable.html"

            if [[ ! -f "$timetable_file" ]]; then
                curl -s "$timetable_url" -H 'Accept-Language: en-US,en;q=0.8' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/45.0.2454.101 Chrome/45.0.2454.101 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Connection: keep-alive' > "$timetable_file"
            fi

            route_stops_timetables=($(xidel -q -e '//div[@class="box butoane-statii"]/a/@href' --data="$timetable_file"))
            # Linia 23B - Triaj - Municipal
            # e.g. http://www.ratbv.ro/route/21/tour/timetable/
#            /route/21/tour/timetable/1/
#            /route/21/tour/timetable/4/
#            /route/21/tour/timetable/6/
#            /route/21/tour/timetable/110/
#            /route/21/tour/timetable/111/
#            /route/21/tour/timetable/119/
#            /route/21/tour/timetable/55/
#            /route/21/tour/timetable/57/
#            /route/21/tour/timetable/58/
#            /route/21/tour/timetable/85/
#            /route/21/tour/timetable/84/

            IFS=';' read -a route_stops_names <<< "$(xidel -q -e '//div[@class="box butoane-statii"]/a/text()' --data="$timetable_file" | awk -F'\n' '{ if (NR > 1) { var=var ";" $1; } else { var=var $1; } } END { print var; }')"
#            Triaj
#            RAT Brasov
#            Autogara 3
#            Sala Sporturilor
#            Bd. Garii
#            Iancu Jianu
#            Plevnei
#            Tudor Vladimirescu
#            Stadionul Tineretului
#            Bartolomeu Gara
#            Municipal

            printf '%s\n' "${route_stops_timetables[@]}"
            printf '%s\n' "${route_stops_names[@]}"

            IFS=';' read -a timetable_service_dates <<< $(xidel -q -e '//table/thead/tr/td[position() > 1]' --data="$timetable_file" | sed 's/^ *//;s/ *$//' | awk -F'\n' '{ if (NR > 1) { var=var ";" $1; } else { var=var $1; } } END { print var; }')
            # Luni - Vineri;Sambata;Duminica;Luni - Vineri (vacanta)

            number_of_service_dates="${#timetable_service_dates[@]}" # 4

            # head -n -1 ignores last line, which for first td of the tr is: Cursele marcate cu culoarea rosie se executa cu autobuz echipat pentru accesul persoanelor cu mobilitate redusa.
            IFS=';' read -a timetable_service_hours <<< $(xidel -q -e '//table/tbody/tr/td[1]' --data="$timetable_file" | head -n -1 | sed 's/^ *//;s/ *$//' | awk -F'\n' '{ if (NR > 1) { var=var ";" $1; } else { var=var $1; } } END { print var; }')
            # 05;06;07;08;09;10;11;12;13;14;15;16;17;18;19;20;21;22;23;00

            number_of_service_hours="${#timetable_service_hours[@]}"

            has_week=false
            has_saturday=false
            has_sunday=false
            has_week_holiday=false

            for rservice in "${!timetable_service_dates[@]}"
            do
                service_name="${timetable_service_dates[$rservice]}"

                if [[ $service_name = 'Ora' ]]; then
                    continue
                fi

                pindex=$(($rservice+2))
                # 0 + 2 = 2
                # 1 + 2 = 3
                # 2 + 2 = 4
                # 3 + 2 = 5

                out=$(xidel -q -e "//table/tbody/tr[position() > 0]/td[$pindex]" --data="$timetable_file" | sed 's/^ *//;s/ *$//' | sed '/^\s*$/d' | grep -oP '\d+' | xargs)

                case $service_name in
                    "Luni - Vineri")
                       if [[ -n "$out" ]]; then
                            has_week=true
                       fi
                    ;;
                    "Sambata")
                       if [[ -n "$out" ]]; then
                            has_saturday=true
                       fi
                    ;;
                    "Duminica")
                       if [[ -n "$out" ]]; then
                            has_sunday=true
                       fi
                    ;;
                    "Luni - Vineri (vacanta)")
                       if [[ -n "$out" ]]; then
                            has_week_holiday=true
                       fi
                    ;;
                    *)
                        #SAT_1_vacanta
                        #SUN_1_vacanta
                        echo "Invalid service name: $route_short_name - $service_name"
                        exit 1
                    ;;
                esac
            done

            # monday,tuesday,wednesday,thursday,friday,saturday,sunday
            declare -a service_dates_array
            service_dates_array=(0 0 0 0 0 0 0)

            declare -a service_dates_array_holiday
            service_dates_array_holiday=(0 0 0 0 0 0 0)

            declare -a service_ids

            # TODO: Find a way to determine these values
            service_start_date=20151231
            service_end_date=20161231

            if [[ $has_week = true ]]; then
              service_id="WEEK_$route_short_name"
              for sdi in "${!service_dates_array[@]}"
              do
                if [[ $sdi -le 4 ]]; then
                        service_dates_array[$sdi]=1
                fi
              done
              echo "$service_id,1,1,1,1,1,0,0,$service_start_date,$service_end_date" >> "$OUTPUT_DIR/calendar.txt"
              service_ids+=($service_id)
            fi

            if [[ $has_saturday = true ]]; then
              service_id="SATURDAY_$route_short_name"
              service_dates_array[5]=1
              service_ids+=($service_id)
              echo "$service_id,0,0,0,0,0,1,0,$service_start_date,$service_end_date" >> "$OUTPUT_DIR/calendar.txt"
            fi

            if [[ $has_sunday = true ]]; then
              service_id="SUNDAY_$route_short_name"
              service_dates_array[6]=1
              service_ids+=($service_id)
              echo "$service_id,0,0,0,0,0,0,1,$service_start_date,$service_end_date" >> "$OUTPUT_DIR/calendar.txt"
            fi

            if [[ $has_week_holiday = true ]]; then
              service_id="WEEK_${route_short_name}_HOLIDAY"

              for sdi in "${!service_dates_array_holiday[@]}"
              do
                if [[ $sdi -le 4 ]]; then
                        service_dates_array_holiday[$sdi]=1
                fi
              done

              service_ids+=($service_id)
              echo "$service_id,1,1,1,1,1,0,0,$service_start_date,$service_end_date" >> "$OUTPUT_DIR/calendar.txt"

              if [[ $has_saturday = true ]]; then
                service_id="SATURDAY_${route_short_name}_HOLIDAY"
                service_ids+=($service_id)
                service_dates_array_holiday[5]=1
                echo "$service_id,0,0,0,0,0,1,0,$service_start_date,$service_end_date" >> "$OUTPUT_DIR/calendar.txt"
              fi

              if [[ $has_sunday = true ]]; then
                 service_id="SUNDAY_${route_short_name}_HOLIDAY"
                service_ids+=($service_id)
                service_dates_array_holiday[6]=1
                echo "$service_id,0,0,0,0,0,0,1,$service_start_date,$service_end_date" >> "$OUTPUT_DIR/calendar.txt"
              fi
            fi

            # ------------------------------------------------------------------------------------------------
            # stop_times.txt

            continue

            stop_counter=1

            echo "Number of stops in route $route_short_name - $route_long_name ~> ${#route_stops_names[@]}"

            # Tours
            for sindex in "${!route_stops_names[@]}"
            do
                route_stop_name="${route_stops_names[$sindex]}"
                route_stop_timetable="${route_stops_timetables[$sindex]}"
                route_stop_url="$BASE_URL$route_stop_timetable"
                route_stop_timetable_filename="$PROCESS_DIR/timetables/${route_short_name}/$((sindex+1))_$route_stop_name.html"
                echo "$route_stop_name, $route_stop_timetable, $route_stop_url"

                # Tour: http://www.ratbv.ro/route/1/tour/timetable/
                # Retour: http://www.ratbv.ro/route/1/retour/timetable/
                if [[ ! -f "$route_stop_timetable_filename" ]]; then
                  curl -s "$route_stop_url" -H 'Accept-Language: en-US,en;q=0.8' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/45.0.2454.101 Chrome/45.0.2454.101 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Connection: keep-alive' > "$route_stop_timetable_filename"
                fi

                for sidx in "${!service_ids[@]}"
                do
                    service_id="${service_ids[$sidx]}"

                    for tidx in "${!timetable_service_hours[@]}"
                    do
                        hour="${timetable_service_hours[$tidx]}"

                        xidel -q -e "//table/tbody/tr[$((tidx+1))]/td[$((sidx+2))]" --data="$route_stop_timetable_filename" | sed 's/^ *//;s/ *$//' | xargs | tr -s ' '
                    done

                    trip_id="$route_short_name/$service_id/"
                    arrival_time="$hour:${minutes[*]}"
                    departure_time=

                    # TODO: Find stop_id instead of using stop name
                    stop_id="$route_stop_name"
                    stop_sequence="$stop_counter"
                    pickup_type=0
                    drop_off_type=0

                    # trip_id,arrival_time,departure_time,stop_id,stop_sequence,pickup_type,drop_off_type
                    # 1/WEE_1/0/0,05:20:00,05:20:00,TRIAJ,1,0,

                    echo "$arrival_time,$departure_time,$stop_id,$stop_sequence,$pickup_type,$drop_off_type" >> "$OUTPUT_DIR/stop_times.txt"
                done

                let stop_counter++
            done

            # Retours

        else # old timetable page format
            # TODO: http://www.ratbv.ro/afisaje/50-dus.html
            # wget -r -k -l 1 -P tmp http://www.ratbv.ro/afisaje/50-dus.html

            # stop names pages
            # dus
            # echo 'http://www.ratbv.ro/afisaje/5-dus.html' | cut -d'/' -f5 | cut -d'.' -f1 | tr '[[:lower:]]' '[[:upper:]]' | sed 's/DUS/dus/' | awk '{ print "http://www.ratbv.ro/afisaje/" $1 "/div_list_ro.html" }'
            # awk -F',' '$7 ~ /afisaje/ {print $7}' routes.txt | cut -d'/' -f5 | cut -d'.' -f1 | tr '[[:lower:]]' '[[:upper:]]' | sed 's/DUS/dus/' | awk '{ print "http://www.ratbv.ro/afisaje/" $1 "/div_list_ro.html" }'

            # intors
            #echo 'http://www.ratbv.ro/afisaje/5-dus.html' | cut -d'/' -f5 | cut -d'.' -f1 | sed 's/dus/intors/g'  | tr '[[:lower:]]' '[[:upper:]]' | sed 's/INTORS/intors/' | awk '{ print "http://www.ratbv.ro/afisaje/" $1 "/div_list_ro.html" }'
            #awk -F',' '$7 ~ /afisaje/ {print $7}' routes.txt | cut -d'/' -f5 | cut -d'.' -f1 | sed 's/dus/intors/g'  | tr '[[:lower:]]' '[[:upper:]]' | sed 's/INTORS/intors/' | awk '{ print "http://www.ratbv.ro/afisaje/" $1 "/div_list_ro.html" }'

            # get actual stop names for route
            # xidel -q -e '//*/a' --data=http://www.ratbv.ro/afisaje/28B-dus/div_list_ro.html | tail -n +2

            # get stop hours
            # awk -F',' '$7 ~ /afisaje/ {print $7}' routes.txt | xargs -L 1 curl -s % | grep -Po '(?<=<frame name="MainFrame" src=").*(?=">)' | awk '{print "http://www.ratbv.ro/afisaje/" $1 }'
            # curl -s 'http://www.ratbv.ro/afisaje/5-dus.html' | grep -Po '(?<=<frame name="MainFrame" src=").*(?=">)' | awk '{print "http://www.ratbv.ro/afisaje/" $1 }'

            # cl1 vs cl2
            # http://www.ratbv.ro/afisaje/25-dus/line_25_1_cl2_ro.html
            # http://www.ratbv.ro/afisaje/25-intors/line_25_1_cl1_ro.html

            true
        fi

        # ------------------------------------------------------------------------------------------------
    done
        
    echo "Number of stops (including duplicates): $(grep -ch code $PROCESS_DIR/stops/tour/json/*.json $PROCESS_DIR/stops/retour/json/*.json | awk 'BEGIN { s=0} { s+=$1} END {print s}')"
    echo "Number of stop (including duplicates): $(wc -l < $OUTPUT_DIR/stops.txt)"
    gawk -F, -i inplace '!seen[$1]++' "$OUTPUT_DIR/stops.txt"
    sed '/^,/d' "$OUTPUT_DIR/stops.txt" | sed '/^\s*$/d' | body sort -k1,1 -t, > "$OUTPUT_DIR/stopsr.txt"
    mv "$OUTPUT_DIR/stopsr.txt" "$OUTPUT_DIR/stops.txt"
    echo "Number of stops (unique): $(wc -l < $OUTPUT_DIR/stops.txt)"
    comm -23 <(tail -n +1 $OUTPUT_DIR/stops.txt | cut -d, -f1 | sort -u) <(tail -n +1 $PREVIOUS_GTFS_FEED_DIR/brasov/stops.txt | cut -d, -f1 | sort -u) > "$PROCESS_DIR/stops/stops_unique_to_this_version.txt"
    comm -13 <(tail -n +1 $OUTPUT_DIR/stops.txt | cut -d, -f1 | sort -u) <(tail -n +1 $PREVIOUS_GTFS_FEED_DIR/brasov/stops.txt | cut -d, -f1 | sort -u) > "$PROCESS_DIR/stops/stops_unique_to_previous_version.txt"
    comm -12 <(tail -n +1 $OUTPUT_DIR/stops.txt | cut -d, -f1 | sort -u) <(tail -n +1 $PREVIOUS_GTFS_FEED_DIR/brasov/stops.txt | cut -d, -f1 | sort -u) > "$PROCESS_DIR/stops/stops_in_both_versions.txt"
    echo "Number of stops unique to this version: $(wc -l < $PROCESS_DIR/stops/stops_unique_to_this_version.txt))"
    echo "Number of stops unique to previous version: $(wc -l < $PROCESS_DIR/stops/stops_unique_to_previous_version.txt))"
    echo "Number of stops included in both versions: $(wc -l < $PROCESS_DIR/stops/stops_in_both_versions.txt))"

    unset route_short_names
    unset route_long_names
    unset route_links
    unset route_styles
done
